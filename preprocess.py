import pandas as pd
import numpy as np
import os
import requests
from sklearn.preprocessing import StandardScaler, OneHotEncoder

# Checking ../Data directory presence
if not os.path.exists('../Data'):
    os.mkdir('../Data')

# Download data if it is unavailable.
if 'nba2k-full.csv' not in os.listdir('../Data'):
    print('Train dataset loading.')
    url = "https://www.dropbox.com/s/wmgqf23ugn9sr3b/nba2k-full.csv?dl=1"
    r = requests.get(url, allow_redirects=True)
    open('../Data/nba2k-full.csv', 'wb').write(r.content)
    print('Loaded.')

data_path = "../Data/nba2k-full.csv"


def clean_data(path: str) -> pd.DataFrame:
    """
    Takes a path to a dataset and returns the cleaned dataset
    :param path: path to the dataset
    :return: cleaned dataset
    """
    
    # Read the file from the listed path
    df = pd.read_csv(path)

    # Update the b_day and draft_year columns as datetime objects
    df['b_day'] = pd.to_datetime(df['b_day'], format='%m/%d/%y')
    df['draft_year'] = pd.to_datetime(df['draft_year'], format='%Y')

    # Replace the missing values in team with "No Team"
    df['team'].fillna('No Team', inplace=True)

    # Take the height in meters
    df['height'] = df['height'].apply(lambda col: col.strip().split()[-1])

    # Take the weight in kg
    df['weight'] = df['weight'].apply(lambda col: col.strip().split()[-2])

    # Remove the dollar symbol from salary
    df['salary'] = df['salary'].apply(lambda val: val.strip().replace('$', ''))

    # Convert the height, weight, and salary to float objects
    df[['height', 'weight', 'salary']] = df[['height', 'weight', 'salary']].astype(float)

    # Categorize the country as "USA" and "Not-USA"
    df['country'] = df['country'].apply(lambda x: 'USA' if x == 'USA' else 'Not-USA')

    # Replace the cells containing "Undrafted" in the draft_round column with the string "0"
    df['draft_round'].replace(['Undrafted'], '0', inplace=True)

    return df


def feature_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Converts a cleaned dataset to a features dataset
    :param df: cleaned dataset
    :return: features dataset
    """

    # Convert the version column to a datetime object
    df['version'] = df['version'].replace(['NBA2k20'], '2020').replace(['NBA2k21'], '2021')
    df['version'] = pd.to_datetime(df['version'], format='%Y')

    # Create the age column by subtracting b_day column from version
    df['age'] = np.ceil((df['version'] - df['b_day']) / np.timedelta64(1, 'Y')).astype('int')

    # Create the experience column by subtracting draft_year from version
    df['experience'] = np.round(
        (df['version'] - df['draft_year']) / np.timedelta64(1, 'Y')).astype('int')

    # Create the body mass index (bmi) using weight / height^2
    df['bmi'] = df['weight'] / df['height'] ** 2

    # Drop version, b_day, draft_year, weight, and height columns
    df.drop(columns=['version', 'b_day', 'draft_year', 'weight', 'height'], inplace=True)

    # Exclude the new columns and salary from dropping in cardinality
    check_cardinal_cols = df.loc[:, ~df.columns.isin(["age", "experience", "bmi", "salary"])].columns

    # Generate list of high cardinality features that will be dropped
    drop_features = [
        index for index, count in df[check_cardinal_cols].nunique().items() if count >= 50
    ]

    # Drop the features if the list isn't empty
    if len(drop_features):
        df.drop(columns=drop_features, inplace=True)

    return df


def multicol_data(df: pd.DataFrame) -> pd.DataFrame:
    """
    Removes multicollinear features from dataset
    :param df: dataframe that needs to be analyzed for multicollinearity
    :return: dataframe that has no multicollinear features
    """

    # Select only the numeric columns from the dataframe
    num_feat_df = df.select_dtypes('number')

    # Calculate the correlation matrix for the numerical features
    corr = num_feat_df.corr()

    # Unpivot the correlation matrix and calculate which features have high correlation coefficients
    unpivot_corr = pd.melt(
        corr.abs().gt(0.5).reset_index(),
        id_vars='index',
        value_vars=corr.columns.values
    )

    # Filter the data to show the multicollinear features
    multicol_feat = unpivot_corr[
        (unpivot_corr['value'] == 1) &
        (unpivot_corr['index'] != unpivot_corr['variable']) &
        (unpivot_corr['index'] != 'salary') &
        (unpivot_corr['variable'] != 'salary')
        ]['index'].unique()

    # Calculate the salary correlation coefficient for the multicollinear features
    salary_multicol_coeff = corr['salary'][multicol_feat].sort_values(ascending=False)

    # Drop the feature that has the lowest correlation coefficient
    df.drop(columns=salary_multicol_coeff.index[-1], inplace=True)

    return df


def transform_data(df: pd.DataFrame) -> tuple:
    """
    Transform the numerical and categorical features
    :param df: dataframe that needs to be transformed
    :return: tuple of features and target variable
    """

    # Separate the dataframe by its type
    numerical_features = df.select_dtypes('number').drop(columns='salary')
    categorical_features = df.select_dtypes('object')

    # Scale the numerical values using StandardScaler
    std_scaler = StandardScaler()
    std_scaler.fit(numerical_features)
    numerical_features[numerical_features.columns] = std_scaler.transform(numerical_features)

    # Encode the categories using OneHotEncoder
    oh_enc = OneHotEncoder(sparse_output=False)
    features = categorical_features.columns.copy()

    # Loop through each feature
    for feature in features:
        # Reshape the features
        reshape_feature = categorical_features[feature].values.reshape(-1, 1)

        # Fit and transform the feature
        oh_enc.fit(reshape_feature)
        transformed = oh_enc.transform(reshape_feature)

        # Drop the feature from the existing dataframe
        categorical_features.drop(columns=feature, inplace=True)

        # Create new dataframe and concatenate with existing dataframe
        categorical_features = pd.concat(
            [categorical_features, pd.DataFrame(transformed, columns=oh_enc.categories_[0])], axis=1
        )

    return pd.concat([numerical_features, categorical_features], axis=1), df['salary']
